package proyectoprog3.controller;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import proyectoprog3.hilos.HiloEscritura;
import proyectoprog3.hilos.HiloLectura;
import proyectoprog3.model.CaracteresEspeciales;
import proyectoprog3.model.Texto;
import proyectoprog3.view.Interfaz;

public class ControllerInterfaz {
    
    private static String resultadoCarga;
    private static String resultadoGenerado;
    private static String cadenaResultado;
    private static String textoOriginal;
    
    public void metodoBoton1() {
       
        
       Interfaz.modificado.setText("");
        try {
            
            JFileChooser chooser = new JFileChooser();
            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            chooser.setAcceptAllFileFilterUsed(false);
            chooser.setFileFilter(new FileNameExtensionFilter("TXT", "txt"));
            chooser.showOpenDialog(null);
            resultadoCarga = chooser.getSelectedFile().getAbsolutePath();
            
            HiloLectura h1 = new HiloLectura(resultadoCarga);
            h1.start();
            h1.join();
            Interfaz.cargado.setText(h1.getResultado());
            Interfaz.cargado.update(Interfaz.cargado.getGraphics());

            //          jTextArea1.setText(Persistencia.leer(entrada, "", ""));
            resultadoGenerado = chooser.getSelectedFile().getParentFile().getAbsolutePath();
            
            char[] caracteresEspeciales = {'|', '/', '*', '#', '"', '^', '$', ',', '-', '¿', '?', '.', '+', '_', '=', '%', '&', '¡', '!', '‘', '~', '\\', '<', '>', '(', ')', '[', ']', '{', '}', ':', ';', '.', ','};
            
            String cadena = Interfaz.cargado.getText();
            textoOriginal = cadena;
            
            String cadenaAux = cadena;
            String[] arrOriginalAux = cadenaAux.split(" ");
            cadena = CaracteresEspeciales.tratamientoCondiciones(cadena.toLowerCase(), caracteresEspeciales, 0);
            
            String[] arrOriginal = cadena.split(" ");
            String[] arrActualizado = cadena.split(" ");
            
            String[] resultado = Texto.inicializarRecorrido(arrOriginal, 0, cadena, arrActualizado, arrOriginalAux);
            cadenaResultado = recorrer(0, resultado, "");
            Interfaz.modificado.setText(cadenaResultado);
            
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ningun archivo");
            
        } catch (InterruptedException ex) {
            JOptionPane.showMessageDialog(null, "Se interrumpio la ejecución");
        }
        
    }
    
    public String recorrer(int i, String[] resultado, String cadenaResultado) {
        if (i > resultado.length - 1) {
            return cadenaResultado;
            
        }else {
            cadenaResultado += resultado[i] + " ";
            return recorrer(i + 1, resultado, cadenaResultado);
        }
        
    }
    
    public void metodoBoton2() {
        
        if (resultadoCarga != null) {
            try {
                JFileChooser chooser = new JFileChooser();
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                chooser.showSaveDialog(null);
                
                resultadoGenerado = chooser.getSelectedFile().getAbsolutePath();
                
                String nombreArchivo = JOptionPane.showInputDialog("Ingrese el nombre del archivo");
                
                if (nombreArchivo.equals("")) {
                    JOptionPane.showMessageDialog(null, "Ingrese un nombre al archivo");
                } else {
                    HiloEscritura h2 = new HiloEscritura(resultadoGenerado + "\\" + nombreArchivo + ".txt",
                            "               TEXTO ORIGINAL               \n\n" + textoOriginal
                            + "               TEXTO MODIFICADO               \n\n" + cadenaResultado);
                    h2.start();
                    h2.join();
                    
                    JOptionPane.showMessageDialog(null, "Archivo generado exitosamente");
                    Interfaz.rutaArchivo.setText(resultadoGenerado + "\\" + nombreArchivo + ".txt");
                }
                
            } catch (NullPointerException ex) {
                JOptionPane.showMessageDialog(null, "No has seleccionado ninguna carpeta");

                Interfaz.rutaArchivo.setText("No has seleccionado ninguna carpeta");
                
            } catch (InterruptedException ex) {
                System.out.println(ex.getMessage());
            }
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar la ruta antes de generar el archivo");
        }
    }
    
}
