package proyectoprog3.hilos;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import proyectoprog3.model.Persistencia;

public class HiloLectura extends Thread {

    private String leerR;
    public String resultado;

    public HiloLectura(String leerR) {
        this.leerR = leerR;

    }

    @Override
    public void run() {
        try {
            System.out.println("Ejecutando");
            BufferedReader entrada = new BufferedReader(new FileReader(this.leerR));
            this.resultado = Persistencia.leer(entrada, "", "");
        } catch (IOException ex) {
            System.out.println("Fin Ejecución");
        }

    }

    public String getResultado() {
        return resultado;
    }
}
