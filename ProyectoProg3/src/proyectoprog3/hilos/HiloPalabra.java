package proyectoprog3.hilos;

import proyectoprog3.model.Palabra;
import proyectoprog3.model.Texto;
import static proyectoprog3.model.Texto.posSalto;
import static proyectoprog3.model.Texto.posSaltoInterfaz;
import proyectoprog3.view.Interfaz;

public class HiloPalabra extends Thread {

    private String palabra;

    public HiloPalabra(String palabra) {
        this.palabra = palabra;
    }

    @Override
    public void run() {

        System.out.print("... Analizando");
        Interfaz.jTextField1.setText(this.palabra);
        if (posSalto != -1 || posSaltoInterfaz != -1) {
            if (Palabra.yaEsPalindroma(this.palabra, "", this.palabra.length() - 1)) {
                System.out.print(", Palindroma, Fin proceso\n");
                Interfaz.jTextField2.setText("Palabra originalmente Palindroma");
                Interfaz.jTextField3.setText(this.palabra);
                Interfaz.modificado.append("\n" + this.palabra + " ");

            } else {
                if (Palabra.esConvertible(this.palabra, 0, 0, ' ')) {
                    System.out.print(", Palabra Convertible, Fin proceso\n");
                    Interfaz.jTextField2.setText("Palabra convertida");
                    Interfaz.jTextField3.setText(Palabra.esPalindroma);
                    Interfaz.modificado.append("\n" + Palabra.esPalindroma + " ");

                } else {
                    System.out.print(", Palabra no Convertible, Fin proceso\n");
                    Interfaz.jTextField2.setText("Palabra no convertida");
                    Interfaz.jTextField3.setText(this.palabra);
                    Interfaz.modificado.append("\n" + this.palabra + " ");
                }
            }
        } else {
            if (Palabra.yaEsPalindroma(this.palabra, "", this.palabra.length() - 1)) {
                System.out.print(", Palindroma, Fin proceso\n");
                Interfaz.jTextField2.setText("Palabra originalmente Palindroma");
                Interfaz.jTextField3.setText(this.palabra);
                Interfaz.modificado.append(this.palabra + " ");

            } else {
                if (Palabra.esConvertible(this.palabra, 0, 0, ' ')) {
                    System.out.print(", Palabra Convertible, Fin proceso\n");
                    Interfaz.jTextField2.setText("Palabra convertida");
                    Interfaz.jTextField3.setText(Palabra.esPalindroma);
                    Interfaz.modificado.append(Palabra.esPalindroma + " ");

                } else {
                    System.out.print(", Palabra no Convertible, Fin proceso\n");
                    Interfaz.jTextField2.setText("Palabra no convertida");
                    Interfaz.jTextField3.setText(this.palabra);
                    Interfaz.modificado.append(this.palabra + " ");
                }
            }
        }
        Interfaz.jTextField1.update(Interfaz.jTextField1.getGraphics());
        Interfaz.jTextField2.update(Interfaz.jTextField2.getGraphics());
        Interfaz.jTextField3.update(Interfaz.jTextField3.getGraphics());
        Interfaz.modificado.update(Interfaz.modificado.getGraphics());

        try {
            HiloPalabra.sleep(2000);
        } catch (InterruptedException ex) {
        }
    }

    public String getPalabra() {
        return palabra;
    }

}
