
package proyectoprog3.hilos;

import java.io.IOException;
import proyectoprog3.model.Persistencia;

public class HiloEscritura extends Thread{ 
   
    private String ruta;
    private String contenido;

    public HiloEscritura(String ruta, String contenido) {
        this.ruta = ruta;
        this.contenido = contenido;
    }
    
    @Override
    public void run(){
      
           System.out.println("Ejecutando");
        try {
            Persistencia.escribirArchivo(this.ruta, this.contenido);
        } catch (IOException ex) {
            System.out.println("Fin Ejecucion");
        }
        
    }
    }
