package proyectoprog3.model;

import java.util.Arrays;
import proyectoprog3.hilos.HiloPalabra;

public class Texto {

    public static int posSalto = -1;
    public static int posSaltoInterfaz = -1;

    public static String[] inicializarRecorrido(String[] arrOriginal, int index, String palabra, String[] arrActualizado, String[] arrOriginalAux) throws InterruptedException {
        Arrays.fill(CaracteresEspeciales.ArregloTildes, "");

        if (index > arrOriginal.length - 1) {
            posSalto = -1;
            posSaltoInterfaz = -1;
            return arrActualizado;
        } else if (arrOriginal[index].length() == 0 || arrOriginal[index].equals("\n")) {
            posSaltoInterfaz = 0;
            arrActualizado[index] = arrOriginal[index];
            return inicializarRecorrido(arrOriginal, index + 1, palabra, arrActualizado, arrOriginalAux);
        } else {
            if (arrOriginal[index].contains("\n")) {
                arrOriginal[index] = posicionSaltoLinea(arrOriginal[index], 0);
            }
            String cadena = CaracteresEspeciales.tratamientoTildes(arrOriginal[index], 0, 0);

            HiloPalabra hiloP = new HiloPalabra(cadena);
            hiloP.start();
            hiloP.join(4000);

            palabra = Palabra.esPalindroma;
            if (!Palabra.esConvertible(cadena, 0, 0, ' ') || Palabra.yaEsPalindroma(cadena, "", cadena.length() - 1) == true) {
                arrActualizado[index] = arrOriginalAux[index];
                posSalto = -1;
                posSaltoInterfaz = -1;
            } else {
                String modificarPalabra = CaracteresEspeciales.retornarTildes(palabra, 0, 0);
                if (posSalto != -1) {
                    arrActualizado[index] = "\n" + modificarPalabra;
                    posSalto = -1;
                    posSaltoInterfaz = -1;
                } else {
                    arrActualizado[index] = modificarPalabra;
                }
            }
            Palabra.esPalindroma = "";
            return inicializarRecorrido(arrOriginal, index + 1, palabra, arrActualizado, arrOriginalAux);
        }
    }

    private static String posicionSaltoLinea(String cadena, int i) {
        if (i > cadena.length() - 1) {
            cadena = cadena.replace("\n", "");
            return cadena;
        } else if (cadena.charAt(i) == '\n') {
            posSalto = i;
        }
        return posicionSaltoLinea(cadena, i + 1);

    }
}
