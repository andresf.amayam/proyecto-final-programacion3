package proyectoprog3.model;

public class CaracteresEspeciales {

    public static String[] ArregloTildes = {"", "", "", "", ""};
    public static char[] tildes = {'á', 'é', 'í', 'ó', 'ú'};
    public static char[] SinTildes = {'a', 'e', 'i', 'o', 'u'};

    private static String reemplazarCaracter(String palabra, char tilde, int i, char[] letras, char sintilde) {
        if (i > palabra.length() - 1) {
            palabra = modificarTilde(letras, tilde, 0, sintilde, "");
            return palabra;
        } else {
            letras[i] = palabra.charAt(i);
        }
        return reemplazarCaracter(palabra, tilde, i + 1, letras, sintilde);
    }

    private static String modificarTilde(char[] letras, char tilde, int i, char sintilde, String palabra) {
        if (i > letras.length - 1) {
            palabra = retornarLetras(letras, 0, "");
            return palabra;
        } else if (letras[i] == sintilde) {
            letras[i] = tilde;
            return modificarTilde(letras, tilde, letras.length, sintilde, palabra);
        }
        return modificarTilde(letras, tilde, i + 1, sintilde, palabra);

    }

    private static String retornarLetras(char[] letras, int i, String palabra) {
        if (i > letras.length - 1) {
            return palabra;
        } else {
            palabra += letras[i];
        }
        return retornarLetras(letras, i + 1, palabra);

    }

    public static String retornarTildes(String palabraOrganizada, int posTildes, int posPalabra) {
        if (posPalabra > palabraOrganizada.length() - 1) {
            return palabraOrganizada;
        } else {
            if (posTildes > ArregloTildes.length - 1) {
                return retornarTildes(palabraOrganizada, 0, posPalabra + 1);
            } else {

                if (ArregloTildes[posTildes].equals("") || ArregloTildes[posTildes].equals(" ")) {
                    return retornarTildes(palabraOrganizada, posTildes + 1, posPalabra);
                } else if (palabraOrganizada.charAt(posPalabra) == ArregloTildes[posTildes].charAt(0)) {
                    char[] letras = new char[palabraOrganizada.length()];
                    if (posTildes == 0) {
                        palabraOrganizada = reemplazarCaracter(palabraOrganizada, 'á', 0, letras, 'a');

                    } else if (posTildes == 1) {
                        palabraOrganizada = reemplazarCaracter(palabraOrganizada, 'é', 0, letras, 'e');

                    } else if (posTildes == 2) {
                        palabraOrganizada = reemplazarCaracter(palabraOrganizada, 'í', 0, letras, 'i');

                    } else if (posTildes == 3) {
                        palabraOrganizada = reemplazarCaracter(palabraOrganizada, 'ó', 0, letras, 'o');

                    } else if (posTildes == 4) {
                        palabraOrganizada = reemplazarCaracter(palabraOrganizada, 'ú', 0, letras, 'u');

                    }
                    ArregloTildes[posTildes] = ArregloTildes[posTildes].replaceFirst(String.valueOf(ArregloTildes[posTildes].charAt(0)), "");

                    return retornarTildes(palabraOrganizada, posTildes + 1, posPalabra);

                } else {
                    return retornarTildes(palabraOrganizada, posTildes + 1, posPalabra);

                }
            }
        }
    }

    public static String tratamientoTildes(String palabra, int pos, int posTildes) {
        if (posTildes > tildes.length - 1) {
            return palabra;
        } else {

            if (pos > palabra.length() - 1) {
                return tratamientoTildes(palabra, 0, posTildes + 1);
            } else if (palabra.charAt(pos) == tildes[posTildes]) {

                ArregloTildes[posTildes] += SinTildes[posTildes];

                palabra = palabra.replaceFirst(String.valueOf(palabra.charAt(pos)), String.valueOf(SinTildes[posTildes]));

                return tratamientoTildes(palabra, pos + 1, posTildes);

            } else {
                return tratamientoTildes(palabra, pos + 1, posTildes);

            }

        }

    }

    public static String tratamientoCondiciones(String texto, char[] caracteresEspeciales, int posCaracteresEspeciales) {
        if (posCaracteresEspeciales > caracteresEspeciales.length - 1) {
            return texto;
        } else {
            if (texto.contains(String.valueOf(caracteresEspeciales[posCaracteresEspeciales]))) {
                texto = texto.replace(String.valueOf(caracteresEspeciales[posCaracteresEspeciales]), "");
            } else {
                return tratamientoCondiciones(texto, caracteresEspeciales, posCaracteresEspeciales + 1);

            }
        }
        return tratamientoCondiciones(texto, caracteresEspeciales, posCaracteresEspeciales + 1);

    }

}
